package grophbench

import (
	"testing"

	"github.com/thcyron/graphs"
)

func BenchmarkThyGraph_access(b *testing.B) {
	g := graphs.NewGraph()
	for n := 0; n < b.N; n++ {
		for i := 0; i < accessTestSize; i++ {
			for j := 0; j < accessTestSize; j++ {
				g.AddEdge(i, j, float64(n))
			}
		}
		var sum float64
		for e := range g.EdgesIter() {
			sum += e.Cost
		}
	}
}

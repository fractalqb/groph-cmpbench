package grophbench

import (
	"fmt"
	"testing"

	"github.com/gyuho/goraph"
)

func BenchmarkGoraph_access(b *testing.B) {
	g := goraph.NewGraph()
	var nodes []goraph.ID
	for i := 0; i < accessTestSize; i++ {
		n := goraph.NewNode(fmt.Sprintf("n%d", i))
		g.AddNode(n)
		nodes = append(nodes, n.ID())
	}
	for n := 0; n < b.N; n++ {
		for _, i := range nodes {
			for _, j := range nodes {
				g.ReplaceEdge(i, j, float64(n))
			}
		}
		var sum float64
		for _, i := range nodes {
			for _, j := range nodes {
				w, _ := g.GetWeight(i, j)
				sum += w
			}
		}
	}
}

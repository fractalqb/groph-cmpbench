package grophbench

import (
	"testing"

	"github.com/yourbasic/graph"
)

func BenchmarkYBgraph_access(b *testing.B) {
	g := graph.New(accessTestSize)
	for n := 0; n < b.N; n++ {
		for i := 0; i < accessTestSize; i++ {
			for j := 0; j < accessTestSize; j++ {
				g.AddCost(i, j, int64(n))
			}
		}
		var sum int64
		for i := 0; i < accessTestSize; i++ {
			for j := 0; j < accessTestSize; j++ {
				sum += g.Cost(i, j)
			}
		}
	}
}

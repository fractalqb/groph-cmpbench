package grophbench

import (
	"testing"

	"git.fractalqb.de/fractalqb/groph/adjmatrix"
)

const accessTestSize = 999

func BenchmarkGrophInt32_access(b *testing.B) {
	g := adjmatrix.NewDInt32(accessTestSize, adjmatrix.I32Del, nil)
	for n := 0; n < b.N; n++ {
		for i := 0; i < accessTestSize; i++ {
			for j := 0; j < accessTestSize; j++ {
				g.SetEdge(i, j, int32(n))
			}
		}
		var sum int32
		for i := 0; i < accessTestSize; i++ {
			for j := 0; j < accessTestSize; j++ {
				w, _ := g.Edge(i, j)
				sum += w
			}
		}
	}
}

func BenchmarkGrophFloat32_access(b *testing.B) {
	g := adjmatrix.NewDFloat32(accessTestSize, nil)
	for n := 0; n < b.N; n++ {
		for i := 0; i < accessTestSize; i++ {
			for j := 0; j < accessTestSize; j++ {
				g.SetEdge(i, j, float32(n))
			}
		}
		var sum float32
		for i := 0; i < accessTestSize; i++ {
			for j := 0; j < accessTestSize; j++ {
				sum += g.Edge(i, j)
			}
		}
	}
}

module git.fractalqb.de/fractalqb/groph-cmpbench

go 1.13

require (
	git.fractalqb.de/fractalqb/groph v0.5.0
	github.com/gyuho/goraph v0.0.0-20171001060514-a7a4454fd3eb
	github.com/thcyron/graphs v0.0.0-20150819191807-c4415928a6c5
	github.com/yourbasic/graph v0.0.0-20170921192928-40eb135c0b26
	gonum.org/v1/gonum v0.8.1
	gopkg.in/yaml.v2 v2.2.7 // indirect
)

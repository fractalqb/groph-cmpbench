package grophbench

import (
	"math"
	"testing"

	"gonum.org/v1/gonum/graph/simple"
)

type GonumNode int64

func (n GonumNode) ID() int64 { return int64(n) }

func BenchmarkGonum_access(b *testing.B) {
	g := simple.NewDirectedMatrix(accessTestSize, 0, 1, math.MaxFloat64)
	for n := 0; n < b.N; n++ {
		for i := GonumNode(0); i < accessTestSize; i++ {
			for j := GonumNode(0); j < i; j++ {
				g.SetEdge(simple.WeightedEdge{F: i, T: j, W: float64(n)})
			}
			for j := i + 1; j < accessTestSize; j++ {
				g.SetEdge(simple.WeightedEdge{F: i, T: j, W: float64(n)})
			}
		}
		var sum float64
		for i := int64(0); i < accessTestSize; i++ {
			for j := int64(0); j < accessTestSize; j++ {
				w, _ := g.Weight(i, j)
				sum += w
			}
		}
	}

}
